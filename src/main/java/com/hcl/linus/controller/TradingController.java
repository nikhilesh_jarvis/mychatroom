package com.hcl.linus.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;




@RestController
@CrossOrigin(origins="*")
public class TradingController {
	
	@RequestMapping(value = "/hello"  )//method = { RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> hello() {		
		return new ResponseEntity<>("Hello World", HttpStatus.OK);
	}
	

}